#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

unordered_map<string, vector<string>> results;
void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

void printTable()
{
	auto iter = results.end();
	iter--;
	//	int size = results.begin()->second.size();
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		cout << endl;
	}
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}


bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	string balanceTemp;
	int balance;

	string availableTemp;
	int available;

	string priceTemp;
	int price;

	int rc;
	string temp;
	
	clearTable();
	temp = "Select balance from accounts where Buyer_id=" + to_string(buyerid);
	rc = sqlite3_exec(db, temp.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	//printTable();

	balanceTemp = results.begin()->second[0];
	//cout << balanceTemp << endl;
	balance = atoi(balanceTemp.c_str());

	temp = "Select available from cars where id=" + to_string(carid);
	clearTable();
	rc = sqlite3_exec(db, temp.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	availableTemp = results.begin()->second[0];
	available = atoi(availableTemp.c_str());

	temp = "Select price from cars where id=" + to_string(carid);
	clearTable();
	rc = sqlite3_exec(db, temp.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	priceTemp = results.begin()->second[0];
	price = atoi(priceTemp.c_str());


	if (available == 0 || price > balance)
	{
		return false;
	}

	
	clearTable();
	balance = balance - price;
	temp = "update accounts set balance=" + std::to_string(balance) + " where buyer_id=" + std::to_string(buyerid);
	rc = sqlite3_exec(db, temp.c_str(), NULL, 0, &zErrMsg);

	temp = "update cars set available=0 where id=" + std::to_string(carid);
	rc = sqlite3_exec(db, temp.c_str(), NULL, 0, &zErrMsg);
	return true;
}


bool balanceTransfer(int from, int to,int amount, sqlite3* db, char* zErrMsg)
{
	string temp;
	int rc;
	string balanceTemp;
	int balanceFrom;
	int balanceTo;

	clearTable();
	temp = "Select balance from accounts where Buyer_id=" + to_string(from);
	rc = sqlite3_exec(db, temp.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}

	balanceTemp = results.begin()->second[0];
	balanceFrom = atoi(balanceTemp.c_str());

	clearTable();
	temp = "Select balance from accounts where Buyer_id=" + to_string(to);
	rc = sqlite3_exec(db, temp.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}

	balanceTemp = results.begin()->second[0];
	balanceTo = atoi(balanceTemp.c_str());

	if (balanceFrom < amount)
	{
		return false;
	}

	temp = "begin transaction";
	rc = sqlite3_exec(db, temp.c_str(), NULL, 0, &zErrMsg);

	temp = "update accounts set balance=" + to_string(balanceFrom - amount) + " where Buyer_id=" + to_string(from);
	rc = sqlite3_exec(db, temp.c_str(), NULL, 0, &zErrMsg);

	temp = "update accounts set balance=" + to_string(balanceTo + amount) + " where Buyer_id=" + to_string(to);
	rc = sqlite3_exec(db, temp.c_str(), NULL, 0, &zErrMsg);

	temp = "commit";
	rc = sqlite3_exec(db, temp.c_str(), NULL, 0, &zErrMsg);
	return true;
}


int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;

	// connection to the database
	rc = sqlite3_open("carsDealer.db", &db);

	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	clearTable();
	rc = sqlite3_exec(db, "select * from accounts", callback, 0, &zErrMsg);
	printTable();
	clearTable();
	rc = sqlite3_exec(db, "select * from cars", callback, 0, &zErrMsg);
	printTable();

	flag = carPurchase(12, 7, db, zErrMsg);

	if (flag)
	{
		cout << "Success purchase" << endl;
	}
	else
	{
		cout << "Fail purchase" << endl;
	}

	flag = carPurchase(1, 1, db, zErrMsg);

	if (flag)
	{
		cout << "Success purchase" << endl;
	}
	else
	{
		cout << "Fail purchase" << endl;
	}
	

	flag = carPurchase(8, 8, db, zErrMsg);

	if (flag)
	{
		cout << "Success purchase" << endl;
	}
	else
	{
		cout << "Fail purchase" << endl;
	}

	flag = balanceTransfer(12, 11, 100000, db, zErrMsg);
	if (flag)
	{
		cout << "Success transfer balance" << endl;
	}
	else
	{
		cout << "Fail transfer balance" << endl;
	}


	clearTable();
	rc = sqlite3_exec(db, "select * from accounts", callback, 0, &zErrMsg);
	printTable();
	clearTable();
	rc = sqlite3_exec(db, "select * from cars", callback, 0, &zErrMsg);
	printTable();


	sqlite3_close(db);

	system("pause");
	return 0;
}